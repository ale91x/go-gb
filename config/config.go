package config

import (
	"github.com/ghodss/yaml"
	"io/ioutil"
	"log"
)

type Cfg struct {
	Url     string `yaml:"url"`
	Depth   int    `yaml:"depth"`
	Workers int    `yaml:"workers"`
}

func LoadConfig(configPaths string, cfg *Cfg) error {
	yamlFile, err := ioutil.ReadFile(configPaths)
	if err != nil {
		log.Printf("Error read file #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &cfg)
	return err
}
