package main

import (
	"awesomeProject/config"
	"awesomeProject/crawler"
	f "awesomeProject/fetcher"
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
)

func main() {
	var wg sync.WaitGroup
	var configPath string
	var IncreaseDepth = make(chan int, 1)

	flag.StringVar(&configPath, "c", "./config.yml", "Path to configFile")
	flag.Parse()
	cfg := new(config.Cfg)
	// Читаем/маршалим конфиг в &cfg
	if err := config.LoadConfig(configPath, cfg); err != nil {
		log.Fatalf("invalid application configuration: %s", err)
	}
	// Создаем контекст с нотифаером.
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop() // Вызываем при получении сигналов в горутине обработки сигналов.

	// Канал пользовательских сигналов.
	usrSigs := make(chan os.Signal, 1)
	signal.Notify(usrSigs, syscall.SIGUSR1)

	fetcher := make(f.ResultFetcher)

	// Канал задач
	urlsChan := make(chan f.Queue, 10000000)

	// Наполнение очереди задачи
	crawler.CrawlerInit(f.Queue{Url: cfg.Url, Depth: cfg.Depth}, fetcher, &IncreaseDepth, urlsChan)

	// Запускаем воркеров
	for i := 1; i < cfg.Workers+1; i++ {
		wg.Add(1)
		go crawler.CrawlerWorker(ctx, fetcher, &IncreaseDepth, urlsChan, &wg, i)
	}

	// Горутина для обработки сигналов USR1 и выхода в случае если воркеры звершили рабооту.
	wg.Add(1)
	go func(ctx context.Context, stop context.CancelFunc, wg *sync.WaitGroup, IncreaseDepth *chan int) {
		defer wg.Done()
		for {
			//loop:
			select {
			case sigU := <-usrSigs:
				switch sigU {
				case syscall.SIGUSR1:
					*IncreaseDepth <- 2
					break
				}
			default:

				if runtime.NumGoroutine() <= 4 {
					// Закрываем каналы сигналов, чтобы оставить ресиверы.
					close(usrSigs)
					// Закрываем функцию нотифаера контекста.
					// stop()
					// Выходим из цикла, вызывается defer wg.Done()
					return
				}
			}
		}

	}(ctx, stop, &wg, &IncreaseDepth)

	wg.Wait()
	close(urlsChan)
	// Печатаем результаты при выходе из main.
	defer func(fetcher f.ResultFetcher) {
		for _, v := range fetcher {
			log.Printf("url: %v, title: %v, count links: %d\n", v.Url, v.Title, v.LenUrls)
		}
		log.Printf("Total url parsed: %d\n", len(fetcher))
		log.Printf("Active goroutine after exits: %d\n", runtime.NumGoroutine())

		var buf [1024 * 1024]byte
		n := runtime.Stack(buf[:], true)
		bs := string(buf[:n])
		log.Printf("\n-----------\n%s\n-----------", bs)
		log.Println("Bye!")
	}(fetcher)
}
