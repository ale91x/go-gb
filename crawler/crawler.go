package crawler

import (
	f "awesomeProject/fetcher"
	"context"
	"log"
	"sync"
)

func CrawlerInit(url f.Queue, fetcher f.Fetcher, IncreaseDepth *chan int, urlsChan chan f.Queue) {
	if url.Depth <= 0 {
		return
	}
	if len(*IncreaseDepth) != 0 {
		url.Depth += <-*IncreaseDepth
	}
	_, _, urls, _ := fetcher.Fetch(url.Url)
	if len(urls) != 0 {
		for _, u := range urls {
			n := new(f.Queue)
			n.Url = u
			n.Depth = url.Depth - 1
			urlsChan <- *n
		}
	}
	return

}

func CrawlerWorker(ctx context.Context, fetcher f.Fetcher, IncreaseDepth *chan int, urlsChan chan f.Queue, wg *sync.WaitGroup, id int) {
	defer wg.Done()
	defer log.Printf("Crawler worker id: %d stopped!\n", id)

	for {
		select {
		case <-ctx.Done():
			log.Printf("Shutdown crawler worker id: %d\n", id)
			return
		case url := <-urlsChan:

			if url.Depth <= 0 {
				break
			}

			if len(*IncreaseDepth) != 0 {
				url.Depth += <-*IncreaseDepth
				log.Printf("Increased depth: %d\n", url.Depth)
			}
			_, _, urls, _ := fetcher.Fetch(url.Url)
			if len(urls) > 0 {
				for _, u := range urls {
					n := new(f.Queue)
					n.Url = u
					n.Depth = url.Depth - 1

					urlsChan <- *n
				}
			}
		default:
			if len(urlsChan) == 0 {
				return
			}
		}
	}

}
