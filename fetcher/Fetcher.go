package fetcher

import (
	"crypto/tls"
	"fmt"
	"golang.org/x/net/html"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

type Queue struct {
	Url   string
	Depth int
}

type Fetcher interface {
	Fetch(url string) (body string, title string, urls []string, err error)
}

type ResultFetcher map[string]*Result

type Result struct {
	Url     string
	Title   string
	Urls    []string
	LenUrls int
}

func (f ResultFetcher) Fetch(url string) (string, string, []string, error) {

	var m sync.RWMutex

	m.RLock()
	if _, ok := f[url]; ok {
		return "", "", nil, fmt.Errorf("%s exist", url)
	}
	m.RUnlock()
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	client := http.Client{
		Timeout: 1 * time.Second,
	}

	resp, err := client.Get(url)
	if err != nil {
		return "", "", nil, err
	}

	bytes, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	defer client.CloseIdleConnections()
	if err != nil {
		return "", "", nil, err
	}

	url, title, urls, err := f.getLinks(bytes, url)
	if err != nil {
		return url, "", nil, err
	}
	m.Lock()
	f[url] = &Result{Url: url, Title: title, Urls: urls, LenUrls: len(urls)}
	m.Unlock()

	return url, title, urls, err
}

func (f ResultFetcher) getLinks(b []byte, _url string) (string, string, []string, error) {
	var links []string
	var title string
	var fetch func(*html.Node)

	doc, err := html.Parse(strings.NewReader(string(b)))

	if err != nil {
		return _url, "", nil, err
	}

	fetch = func(n *html.Node) {
		if n.Type == html.ElementNode {
			for _, a := range n.Attr {

				if a.Key == "title" {
					title = a.Val
				}

				if a.Key == "href" {
					u, e := url.Parse(a.Val)
					if e == nil && u.Scheme == "https" && u.Host != "" {
						if u.String() != _url {
							links = append(links, u.String())
						}
					}
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			fetch(c)
		}
	}
	fetch(doc)
	return _url, title, links, nil
}
